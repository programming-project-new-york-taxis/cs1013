final int SCREENX = 800;
final int SCREENY = 600;
final int GAP= SCREENY/5;

final int WIDGET_LENGTH = SCREENX/4;
final int WIDGET_WIDTH= SCREENX/20;
final int WIDGET_X = (SCREENX/2)-(WIDGET_LENGTH/2);
final int WIDGET_Y= GAP;

final int  EVENT_BUTTON1  =  1;  
final int  EVENT_BUTTON2  =  2;  
final int EVENT_BUTTON3 = 3;
final int EVENT_BUTTON4 = 4;
final int EVENT_BUTTON5 = 5;
final int EVENT_BUTTON6 = 6;
final int EVENT_BUTTON7 = 7;
final int  EVENT_NULL  =  0;  
final color red = color(255, 0, 0);
final color green =color(0, 255, 0);
final color blue =color(20, 55, 255);
final color grey = color(190, 190, 190);
final color white = color(255);

final color firstBar = color(255, 0, 0);
final color secondBar =color(0, 255, 0);
final color thirdBar =color(0, 0, 255);
