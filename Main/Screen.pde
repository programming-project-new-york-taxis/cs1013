// Author/s: Mainly Evan O'Sullivan handled the implementation of the screen. Peter O'Learly added in the button images. 


class Screen {
  
    color screenColor; 
    int screenNo;
    Widget buttonWidget, moveButton;
    int x;
    int y;
    ArrayList widgetList;
    PImage taxi = loadImage("newYork.jpg");
    PImage skyline = loadImage("Skyline.jpg");
    PImage buttonImage1 = loadImage ("biscuitCakes.png");
    PImage buttonImage2 = loadImage ("map.png");
    PImage buttonImage3 = loadImage ("pickup.png");
    PImage buttonImage4 = loadImage ("dropoff.png");
    PImage buttonImage5 = loadImage ("Main Menu.png");
    PImage buttonImage6 = loadImage ("medallion.png");
    PImage buttonImage7 = loadImage ("Taxi Vendor.png");
    PImage buttonImage8 = loadImage ("ratecode.png");
    PImage buttonImage9 = loadImage ("back.png");
    PImage buttonImage10= loadImage ("Distance.png");
    PImage buttonImage11 = loadImage ("time.png");
    PImage buttonImage12 = loadImage ("Passengers.png");
    PImage buttonImage13 = loadImage ("Forward.png");
    PImage buttonImage14 = loadImage ("time.png");
    PImage buttonImage15 = loadImage ("Main Menu.png");
    PImage buttonImage16 = loadImage ("Midnight - 6am.png");
    PImage buttonImage17 = loadImage ("6am - Midday.png");
    PImage buttonImage18 = loadImage ("Midday - 6pm.png");
    PImage buttonImage19 = loadImage ("6pm - Midnight.png");
    
    Screen(int screenNo){
      
      widgetList  =  new  ArrayList();
      //   widgetN = new Widget(xPosition, yPosition, xSize, ySize, 
      //   widgetString, widgetColor, widgetFont, eventNumber);
      widget1  =  new  Widget(WIDGET_X, WIDGET_Y, WIDGET_LENGTH, WIDGET_WIDTH, 
      "Search", grey, stdFont, EVENT_BUTTON1);  
      widget2  =  new  Widget(WIDGET_X, WIDGET_Y*2, WIDGET_LENGTH, WIDGET_WIDTH, 
      "Map", grey, stdFont, EVENT_BUTTON2);
      widget3  =  new  Widget(WIDGET_X, WIDGET_Y, WIDGET_LENGTH, WIDGET_WIDTH, 
      "Pickup", grey, stdFont, EVENT_BUTTON3);  
      widget4  =  new  Widget(WIDGET_X, WIDGET_Y*2, WIDGET_LENGTH, WIDGET_WIDTH, 
      "DropOff", grey, stdFont, EVENT_BUTTON4);
      widget5  =  new  Widget(10, 10, WIDGET_LENGTH, WIDGET_WIDTH, 
      "Main Screen", grey, stdFont, EVENT_BUTTON5);
      widget6  =  new  Widget(WIDGET_X, WIDGET_Y, WIDGET_LENGTH, WIDGET_WIDTH, 
      "Medallion", grey, stdFont, EVENT_BUTTON6);  
      widget7  =  new  Widget(WIDGET_X, WIDGET_Y*2, WIDGET_LENGTH, WIDGET_WIDTH, 
      "Taxi Vendor", grey, stdFont, EVENT_BUTTON24);
      widget8  =  new  Widget(WIDGET_X, WIDGET_Y*3, WIDGET_LENGTH, WIDGET_WIDTH, 
      "Rate Code", grey, stdFont, EVENT_BUTTON25);
      widget9  =  new  Widget(10, 10, WIDGET_LENGTH, WIDGET_WIDTH, 
      "Back", grey, stdFont, EVENT_BUTTON9);
      widget10  =  new  Widget(WIDGET_X, WIDGET_Y, WIDGET_LENGTH, WIDGET_WIDTH, 
      "Distance", grey, stdFont, EVENT_BUTTON10);  
      widget11  =  new  Widget(WIDGET_X, WIDGET_Y*2, WIDGET_LENGTH, WIDGET_WIDTH, 
      "Time", grey, stdFont, EVENT_BUTTON11);
      widget12  =  new  Widget(WIDGET_X, WIDGET_Y*3, WIDGET_LENGTH, WIDGET_WIDTH, 
      "Passengers", grey, stdFont, EVENT_BUTTON12);
      widget13  =  new  Widget(290, 10, WIDGET_LENGTH, WIDGET_WIDTH, 
      "Forward", grey, stdFont, EVENT_BUTTON9);
      widget14  =  new  Widget(10, 10, WIDGET_LENGTH, WIDGET_WIDTH, 
      "Back", grey, stdFont, EVENT_BUTTON2);
      widget15  =  new  Widget(10, 10, WIDGET_LENGTH, WIDGET_WIDTH, 
      "Main menu", grey, stdFont, EVENT_BUTTON6);
      widget16  =  new  Widget(WIDGET_X, WIDGET_Y, WIDGET_LENGTH, WIDGET_WIDTH, 
      "12am - 6am", grey, stdFont, EVENT_BUTTON16);
      widget17  =  new  Widget(WIDGET_X, WIDGET_Y*2, WIDGET_LENGTH, WIDGET_WIDTH, 
      "6am - 12 noon", grey, stdFont, EVENT_BUTTON17);
      widget18  =  new  Widget(WIDGET_X, WIDGET_Y*3, WIDGET_LENGTH, WIDGET_WIDTH, 
      "12 noon - 6pm", grey, stdFont, EVENT_BUTTON18);
      widget19  =  new  Widget(WIDGET_X, WIDGET_Y*4, WIDGET_LENGTH, WIDGET_WIDTH, 
      "6pm - 12 midnight", grey, stdFont, EVENT_BUTTON19);
  
      // The following 4 widgets are of the same name as the 4 above but deal with dropoff instead of pickup
  
      widget20  =  new  Widget(WIDGET_X, WIDGET_Y, WIDGET_LENGTH, WIDGET_WIDTH, 
      "12am - 6am", grey, stdFont, EVENT_BUTTON20);
      widget21  =  new  Widget(WIDGET_X, WIDGET_Y*2, WIDGET_LENGTH, WIDGET_WIDTH, 
      "6am - 12 noon", grey, stdFont, EVENT_BUTTON21);
      widget22  =  new  Widget(WIDGET_X, WIDGET_Y*3, WIDGET_LENGTH, WIDGET_WIDTH, 
      "12 noon - 6pm", grey, stdFont, EVENT_BUTTON22);
      widget23  =  new  Widget(WIDGET_X, WIDGET_Y*4, WIDGET_LENGTH, WIDGET_WIDTH, 
      "6pm - 12 midnight", grey, stdFont, EVENT_BUTTON23);
  
      widgetList  =  new  ArrayList();         
  
  
      this.screenColor = screenColor;
      this.screenNo = screenNo;
      x=SCREENX;
      y=SCREENY;
      if (screenNo==0) // main screen
      {
        widgetList.add(widget1);
        widgetList.add(widget2);
      } 
      if ((screenNo==1)) // medallion, rate code, taxi vendor
      {
        widgetList.add(widget6);
        widgetList.add(widget7);
        widgetList.add(widget8);
        widgetList.add(widget5);
      }
      if (screenNo==2) //map choice
      {
        widgetList.add(widget3);
        widgetList.add(widget4);
        widgetList.add(widget5);
      }
      if ((screenNo==3)||(screenNo==4)) //maps
      {
        if(map1!=null)
        {
        map1.draw();
        }
        widgetList.add(widget14);
      }
      if (screenNo==5) //search screen
      {
      }
      if (screenNo==6)// distance,time,passengers
      {
        widgetList.add(widget5);
        widgetList.add(widget10); 
        widgetList.add(widget11);
        widgetList.add(widget12);
        widgetList.add(widget15);
      }
      if ((screenNo==7)||(screenNo==8)||(screenNo==9)) //graph pages
      {
        widgetList.add(widget9);
      }
      if (screenNo==10) // pickup times
      {
        widgetList.add(widget14);
        widgetList.add(widget16); 
        widgetList.add(widget17); 
        widgetList.add(widget18); 
        widgetList.add(widget19);
      }
      if ( screenNo ==11) // dropooff times
      {
        widgetList.add(widget14);
        widgetList.add(widget20); 
        widgetList.add(widget21); 
        widgetList.add(widget22); 
        widgetList.add(widget23);
      }
    }
    int getEvent(int mX, int mY)
    {
      int event;
      int temp;
      for (int i=0; i<widgetList.size (); i++)
      {
        Widget  aWidget  =  (Widget)  widgetList.get(i); 
        if ( aWidget.getEvent(mouseX, mouseY) != 0) {
          return aWidget.getEvent(mouseX, mouseY);
        }
      }
      return EVENT_NULL;
    }
  
  
    void draw()
    {
      //println(screenNo);
      if (this.screenNo==0)
      {
        background(taxi);
        for (int i=0; i<widgetList.size (); i++)
        {
          Widget  aWidget  =  (Widget)  widgetList.get(i); 
          aWidget.draw();
          image(buttonImage1, WIDGET_X, WIDGET_Y);
          image(buttonImage2, WIDGET_X, WIDGET_Y*2);
        }
      } else if (this.screenNo==1)
      {
        image(buttonImage5, 10, 10);
        image(buttonImage6, WIDGET_X, WIDGET_Y);
        image(buttonImage7, WIDGET_X, WIDGET_Y*2);
        image(buttonImage8, WIDGET_X, WIDGET_Y*3);
      
    } else if (this.screenNo==2)
  {
    background(skyline);
    image(buttonImage5, 10, 10);
    image(buttonImage3, WIDGET_X, WIDGET_Y);
    image(buttonImage4, WIDGET_X, WIDGET_Y*2);
  } else if ((this.screenNo==3)||(this.screenNo==4))
  {
    for (int i=0; i<widgetList.size (); i++)
    {
      Widget  aWidget  =  (Widget)  widgetList.get(i); 
      aWidget.draw();
      map1.draw();
      image(buttonImage9, 10, 10);
    }
  } else if ( this.screenNo==5)
  {
    background(skyline);
    for (int i=0; i<widgetList.size (); i++)
    {
      Widget  aWidget  =  (Widget)  widgetList.get(i); 
      aWidget.draw();  
  }
   input.draw(); 
  }
  else if (this.screenNo==6)
  {
  background(skyline);
  for (int i=0; i<widgetList.size (); i++)
    {
      Widget  aWidget  =  (Widget)  widgetList.get(i); 
      aWidget.draw();
    }
    image(buttonImage5, 10, 10);
    image(buttonImage10, WIDGET_X, WIDGET_Y);
    image(buttonImage11, WIDGET_X, WIDGET_Y*2);
    image(buttonImage12, WIDGET_X, WIDGET_Y*3);
    
  }

  else if (this.screenNo==7)
  {
    for (int i=0; i<widgetList.size (); i++)
    {
      Widget  aWidget  =  (Widget)  widgetList.get(i); 
      aWidget.draw();
      barChart1.draw();
      image(buttonImage9, 10, 10);
    }
  } else if (this.screenNo==8)
  {
    for (int i=0; i<widgetList.size (); i++)
    {
      Widget  aWidget  =  (Widget)  widgetList.get(i); 
      aWidget.draw();
       barChart2.draw();
      image(buttonImage9, 10, 10);
    }
  } else if (this.screenNo==9)
  {
    for (int i=0; i<widgetList.size (); i++)
    {
      Widget  aWidget  =  (Widget)  widgetList.get(i); 
      aWidget.draw();
      barChart3.draw();
      image(buttonImage9, 10, 10);
    }
  } else if ( (this.screenNo==10) || (this.screenNo==11))
  {
    for (int i=0; i<widgetList.size (); i++)
    {
      Widget  aWidget  =  (Widget)  widgetList.get(i); 
      aWidget.draw();
      image(buttonImage9, 10, 10);
      image(buttonImage16, WIDGET_X, WIDGET_Y);
      image(buttonImage17, WIDGET_X, WIDGET_Y*2);
      image(buttonImage18, WIDGET_X, WIDGET_Y*3);
      image(buttonImage19, WIDGET_X, WIDGET_Y*4);
      
    }
  } else
  {
    background(skyline);
    for (int i=0; i<widgetList.size (); i++)
    {
      Widget  aWidget  =  (Widget)  widgetList.get(i); 
      aWidget.draw();
    }
  }
  }
  }
