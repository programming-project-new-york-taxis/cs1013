// Author/s: Sam Green and Peter O'Leary handled creating a bar chart.

class Chart {
  String label1;
  String label2;
  String label3;
  String heading;
  float value1;
  float value2;
  float value3;

  Chart(String label1, String label2, String label3, String heading, float value1, float value2, float value3) {

    this.label1 = label1;
    this.label2 = label2;
    this.label3 = label3;
    this.heading = heading;
    this.value1 = value1;
    this.value2 = value2;
    this.value3 = value3;
  }


  void draw() {
    size (SCREENX, SCREENY);
    background (255);
    int xaxis = SCREENX/8 + 15;
    int yaxis = SCREENY/6-1;
    int yaxisbottom = SCREENY/6+(SCREENY/6*4);
    int xAxisLength = 442;
    int yAxisHeight = 405;



    // finds which of the value is the biggest
    float biggestValue = 0;
    if (value1>value2) {
      biggestValue = value1;
    } else if (value1<value2) {
      biggestValue = value2;
    }
    if (biggestValue<value3) {
      biggestValue = value3;
    }

    float barValue1 = (value1/biggestValue)*yAxisHeight;
    float barValue2 = (value2/biggestValue)*yAxisHeight;
    float barValue3 = (value3/biggestValue)*yAxisHeight;

    //for drawing chart
    int gap = 10;
    int axisSize = 2;
    int barWidth = 140;
    int bar1Start = xaxis+axisSize;

    int bar2Start = bar1Start+gap+barWidth;
    int bar3Start = bar2Start+gap+barWidth;

    // rect(xstart, ystart, xamount, yamount)
    fill(0);
    rect(xaxis, yaxis, 2, yAxisHeight); // y axis
    rect(xaxis, yaxisbottom, xAxisLength, 2); // x axis


    // y increments
    float increment1 = yAxisHeight;
    //float increment2 = (yAxisHeight-yAxisHeight%50);
    float increment3 = increment1/5*4;
    float increment4 = increment1/5*3;
    float increment5 = increment1/5*2;
    float increment6 = increment1/5*1;
    rect(xaxis-2, yaxisbottom-increment1, 4, 2);
    //rect(xaxis-2, yaxisbottom-increment2, 4, 2);
    rect(xaxis-2, yaxisbottom-increment3, 4, 2);
    rect(xaxis-2, yaxisbottom-increment4, 4, 2);
    rect(xaxis-2, yaxisbottom-increment5, 4, 2);
    rect(xaxis-2, yaxisbottom-increment6, 4, 2);

    /* alternative increment method
     float increment1 = yAxisHeight;
     float increment2 = (yAxisHeight-yAxisHeight%50);
     float increment3 = increment2/5*4;
     float increment4 = increment2/5*3;
     float increment5 = increment2/5*2;
     float increment6 = increment2/5*1;
     */

    //text(string, x, y) TEXT LABELS
    //println(biggestValue);
    text(biggestValue, xaxis-50, yaxisbottom-increment1);
    //text((yAxisHeight-yAxisHeight%50), xaxis-50, yaxisbottom-increment2);
    text(biggestValue/5*4, xaxis-50, yaxisbottom-increment3);
    text(biggestValue/5*3, xaxis-50, yaxisbottom-increment4);
    text(biggestValue/5*2, xaxis-50, yaxisbottom-increment5);
    text(biggestValue/5*1, xaxis-50, yaxisbottom-increment6);

    // x axis labels
    float barMiddle = .3*barWidth; 
    text(label1, (bar1Start+barMiddle), yaxisbottom+20);
    text(label2, (bar2Start+barMiddle), yaxisbottom+20);
    text(label3, (bar3Start+barMiddle), yaxisbottom+20);
    //textSize(30.0);
    text(heading, bar2Start, yaxis -20);  

    // DRAWING BARS
    fill(firstBar);
    rect(bar1Start, yaxisbottom-barValue1, barWidth, barValue1-1);
    fill(secondBar);
    rect(bar2Start, yaxisbottom-barValue2, barWidth, barValue2-1);
    fill(thirdBar);
    rect(bar3Start, yaxisbottom-barValue3, barWidth, barValue3-1);
  }
}

