// Author/s: Mainly Sean Durban handled the implementation of data extraction. 


import de.bezier.data.*;

class Data 
{
  PApplet parent;
  ArrayList<Taxi> taxiData;

  //Pickup arrayLists for each hour
  ArrayList <Float> pickupPeriod1 = new ArrayList <Float>();  //12-6am
  ArrayList <Float> pickupPeriod2 = new ArrayList <Float>();  //6-12 noon
  ArrayList <Float> pickupPeriod3 = new ArrayList <Float>();  //12-6pm
  ArrayList <Float> pickupPeriod4 = new ArrayList <Float>();  //6pm-12 midnight

  //Dropoff arrayLists for each hour.
  ArrayList <Float> dropOffPeriod1 = new ArrayList <Float>();  //12-6am
  ArrayList <Float> dropOffPeriod2 = new ArrayList <Float>();  //6-12 noon
  ArrayList <Float> dropOffPeriod3 = new ArrayList <Float>();  //12-6pm
  ArrayList <Float> dropOffPeriod4 = new ArrayList <Float>();  //6pm - 12 midnight

  float period1Start =0;
  float period2Start =6;
  float period3Start =12;
  float period4Start =18;


  Data(PApplet parent)
  {
    this.parent=parent;
    XlsReader reader;


    int columnNo = query-1;          //Based on queries always being >0
    String file;
    ArrayList<Taxi> taxiData = new ArrayList<Taxi>();
    for (int currentFileNo=1; currentFileNo<2; currentFileNo++)//Currently implemented so it will work with the first file only. (Data1.1)
    {

      file="Data1."+currentFileNo+".xls";
      reader = new XlsReader(parent, file);
      println(file);
      for (int row=1; row<XLS_ROW_LIMIT; row++)        //65537 row limit of xls file
      {

        String medallion = reader.getString(row, 0);
        String hackLicense = reader.getString(row, 1);
        String vendor = reader.getString(row, 2);
        int rateCode = reader.getInt(row, 3);
        String storeNFwd = " ";
        Float pickupDateTime = reader.getFloat(row, 5);
        Float dropoffDateTime = reader.getFloat(row, 6);
        Float passengers = reader.getFloat(row, 7);
        Float time = reader.getFloat(row, 8);
        Float distance = reader.getFloat(row, 9);
        Float pickupLong= reader.getFloat(row, 10);
        Float pickupLat=reader.getFloat(row, 11);
        Float dropoffLong = reader.getFloat(row, 12);
        Float dropoffLat = reader.getFloat(row, 13);

        Taxi taxiToAdd = new Taxi(medallion, hackLicense, vendor, rateCode, storeNFwd, pickupDateTime, dropoffDateTime, passengers, 
        time, distance, pickupLong, pickupLat, dropoffLong, dropoffLat);
        taxiData.add(taxiToAdd);
      }
    }

    this.taxiData= taxiData;
    println(taxiData.size());
  }

  ArrayList<Taxi> getTaxiData()
  {
    return this.taxiData;
  }
  ArrayList<Trip> getUserSearch(int query, String userSearch)
  {
 
    ArrayList<Trip> dataSearched = new ArrayList<Trip>();

    ArrayList<Taxi> taxiData = this.taxiData;//Array list of all taxis
    Taxi taxi = null;
    for (int index =0; index<taxiData.size (); index++)  //13 being amount of values in a column. ie to get back to this value in the array
    {
      String currentQueryValue;
      Taxi currentTaxi = taxiData.get(index);
      if (query==1) //Medallion
      {
        currentQueryValue=currentTaxi.getMedallion();
      } else if (query==2) //Vendor
      {
        currentQueryValue= currentTaxi.getVendor();
      } else  //Rate code
      {
        int rateCode = currentTaxi.getRateCode();
        currentQueryValue= Integer.toString(rateCode);
      }
      boolean invalidTrip=false; 
      if (currentQueryValue.equals(userSearch))
      {
        
        ArrayList <Float> tripAttributes = new ArrayList <Float>();
        for (int attributeNo=1; attributeNo<10; attributeNo++)                    //store_and_fwd_flag-->dropoff_latitude
        {
          
          Float currentAttribute= currentTaxi.getCurrentAttribute(attributeNo);
          if (currentAttribute==0)                  //To catch empty attributes
          {
            invalidTrip=true;
          }
          tripAttributes.add(currentAttribute);
        }
        if (invalidTrip==false)            //All trips with invalid attributes are discarded
        {
          Trip newTrip = new Trip(tripAttributes.get(0), tripAttributes.get(1), tripAttributes.get(2), tripAttributes.get(3), tripAttributes.get(4), 
          tripAttributes.get(5), tripAttributes.get(6), tripAttributes.get(7), tripAttributes.get(8) );
          dataSearched.add(newTrip);
        }
      }
    }
    return dataSearched;
  }




  void getMapData()
  {
    ArrayList<Taxi> taxiData = this.taxiData;//Array list of all taxis

    for (int index=0; index<1000; index++) //Sorts 1000 different trips into different time periods. Can be changed.
    {
      Taxi currentTaxi = taxiData.get(index);
      float pickupTime = currentTaxi.getPickupTime();
      float dropoffTime = currentTaxi.getDropoffTime();
      if (pickupTime!=0&&dropoffTime!=0)    //To catch invalid/empty times.
      {
        float pickupLongitude = currentTaxi.getPickupLong();
        float pickupLatitude = currentTaxi.getPickupLat();
        float dropoffLongitude = currentTaxi.getDropoffLong();
        float dropoffLatitude = currentTaxi.getDropoffLat();
        if (pickupLongitude!=0&&pickupLatitude!=0&&dropoffLongitude!=0&&dropoffLatitude!=0)
        {
          pickupTime= (pickupTime-(int)pickupTime)*24;  //To get time part of serial number. As a ratio of 24 hours eg) 6:00 (am) being 0.25 and 24:00 being 1.
          dropoffTime= (dropoffTime-(int)dropoffTime)*24;  //To get time part of serial number.
          updatePickupArrayList(pickupTime, pickupLongitude, pickupLatitude);
          updateDropoffArrayList(dropoffTime, dropoffLongitude, dropoffLatitude);
        }
      }
    }
  }



  ArrayList<Float> getAverageDistance(ArrayList<Trip> trips)   
  {
    ArrayList <Float> distanceData= new ArrayList <Float>();
    Float shortestDistance=0.0; 
    Float longestDistance=0.0; 
    Float averageDistance=0.0;
    if (trips.size()>0)
    {
      for (int index=0; index<trips.size (); index++)
      {
        Trip currentTrip= trips.get(index);
        Float tripDistance=currentTrip.getDistance();
        if (tripDistance<shortestDistance || shortestDistance==0)        //Or bit is to catch the first value and set it to the shortest.
        {
          shortestDistance=tripDistance;
        }
        if (tripDistance>longestDistance)
        {
          longestDistance=tripDistance;
        }
        averageDistance+=tripDistance;
      }
    }
    averageDistance=averageDistance/trips.size();
    distanceData.add(shortestDistance);
    distanceData.add(averageDistance);
    distanceData.add(longestDistance);
    return distanceData;
  }
  ArrayList<Float> getAverageTime(ArrayList<Trip> trips)  //Works pretty much the same as average distance
  {                                                        //Returns an arrayList of float values representing minutes.
    ArrayList <Float> timeData= new ArrayList <Float>();
    Float shortestTime=0.0; 
    Float longestTime=0.0; 
    Float averageTime=0.0;

    for (int index=0; index<trips.size (); index++)
    {


     
        Trip currentTrip= trips.get(index);
        Float tripTime=(currentTrip.getTime())/60;
        if (tripTime<shortestTime || shortestTime==0)        //Or bit is to catch the first value and set it to the shortest.
        {
          shortestTime=tripTime;
        }
        if (tripTime>longestTime)
        {
          longestTime=tripTime;
        }
        averageTime+=tripTime;
      }
      averageTime= averageTime/trips.size();
      timeData.add(shortestTime);
      timeData.add(averageTime);
      timeData.add(longestTime);
    
    return timeData;
  }

  ArrayList<Float> getTotalPassengers(ArrayList<Trip> trips)
  {
    ArrayList<Float> passengerData = new ArrayList<Float>();
    Float lowestPassengers=0.0;
    Float highestPassengers=0.0;
    Float averagePassengers=0.0;

    for (int index=0; index<trips.size (); index++)



    {
      Trip currentTrip = trips.get(index);
      Float passengers=currentTrip.getPassengerNo();
      if (passengers<lowestPassengers || lowestPassengers==0)        //Or bit is to catch the first value and set it to the shortest.
      {
        lowestPassengers=passengers;
      }
      if (passengers>highestPassengers)
      {
        highestPassengers=passengers;
      }




      averagePassengers+=passengers;
    }
    averagePassengers=averagePassengers/trips.size();
    passengerData.add(lowestPassengers);
    passengerData.add(averagePassengers);
    passengerData.add(highestPassengers);
    return passengerData;
  }

  ArrayList<Float> getpickUpLong(ArrayList<Trip> trips )

  {
    ArrayList<Float> longData = new ArrayList<Float>();

    for (int index=0; index<trips.size (); index++)
    {
      Trip currentTrip = trips.get(index);
      Float pickUpLong=currentTrip.getPickUpLong();
      longData.add(pickUpLong);
    }
    return longData;
  }

  ArrayList<Float> getpickUpLat(ArrayList<Trip> trips)
  {
    ArrayList<Float> latData = new ArrayList<Float>();


    for (int index=0; index<trips.size (); index++)
    {
      Trip currentTrip = trips.get(index);
      Float pickUpLat=currentTrip.getPickUpLat();
      latData.add(pickUpLat);
    }
    return latData;
  }

  void updateDropoffArrayList(float time, float longitude, float latitude)
  {
    if (time>=period1Start && time<period2Start)
    {
      dropOffPeriod1.add(latitude);
      dropOffPeriod1.add(longitude);
      
    } else if (time>=period2Start && time< period3Start)
    {

      dropOffPeriod2.add(latitude);
      dropOffPeriod2.add(longitude);
    } else if (time>=period3Start && time<period4Start)
    {

      dropOffPeriod3.add(latitude);
       dropOffPeriod3.add(longitude);
    } else if (time>period4Start)
    {

      dropOffPeriod4.add(latitude);
       dropOffPeriod4.add(longitude);
    }
  } 
  void updatePickupArrayList(float time, float longitude, float latitude)
  {
    if (time>=period1Start && time<period2Start&&pickupPeriod1.size()<200)
    {
      pickupPeriod1.add(longitude);
      pickupPeriod1.add(latitude);
    } else if (time>=period2Start && time< period3Start)
    {
      pickupPeriod2.add(longitude);
      pickupPeriod2.add(latitude);
    } else if (time>=period3Start && time<period4Start)
    {
      pickupPeriod3.add(longitude);
      pickupPeriod3.add(latitude);
    } else if (time>period4Start)
    {
      pickupPeriod4.add(longitude);
      pickupPeriod4.add(latitude);
    }
  }

  ArrayList<Float> getDropOffPeriod1()
  {
    return this.dropOffPeriod1;
  }
  ArrayList<Float> getDropOffPeriod2()
  {
    return this.dropOffPeriod2;
  }
  ArrayList<Float> getDropOffPeriod3()
  {
    return this.dropOffPeriod3;
  }
  ArrayList<Float> getDropOffPeriod4()
  {
    return this.dropOffPeriod4;
  }
  ArrayList<Float> getPickupPeriod1()
  {
    return this.pickupPeriod1;
  }
  ArrayList<Float> getPickupPeriod2()
  {
    return this.pickupPeriod2;
  }

  ArrayList<Float> getPickupPeriod3()
  {
    return this.pickupPeriod3;
  }

  ArrayList<Float> getPickupPeriod4()
  {
    return this.pickupPeriod4;
  }




  void setup() {
  }
  void draw() {
  }
}



//**OLD CONSTRUCTOR **
//
//Data(PApplet parent, int query, String userSearch) //This object will have an arrayList of trip data as an attribute.
//{
//  this.parent = parent;
//
//
//  XlsReader reader;
//
//  int columnNo = query-1;          //Based on queries always being >0
//  String file;
//  int pickupColumn=5;
//  int dropoffColumn=6;
//  int longColumn=10;
//  int latColumn=11;
//
//
//  for (int currentFileNo=1; currentFileNo<2; currentFileNo++)//Currently implemented so it will work with the first file only. (Data1.1)
//  {
//
//    file="Data1."+currentFileNo+".xls";
//    println(file);
//    reader = new XlsReader(parent, file );
//    boolean invalidTrip=false; 
//    for (int row=1; row<XLS_ROW_LIMIT; row++)        //65537 row limit of xls file
//    {
//      invalidTrip=false;                                
//      String currentCellValue=reader.getString(row, columnNo);    //Gets the value at [row,columnNo]
//      if (currentCellValue.equals(userSearch))
//      { 
//        ArrayList <Float> tripAttributes = new ArrayList <Float>();
//        for (int column=5; column<14; column++)                    //store_and_fwd_flag-->dropoff_latitude
//        {
//          Float currentAttribute= reader.getFloat(row, column);
//          if (currentAttribute==0)                  //To catch empty attributes
//          {
//            invalidTrip=true;
//          }
//          tripAttributes.add(currentAttribute);
//        }
//        if (invalidTrip==false)            //All trips with invalid attributes are discarded
//        {
//          Trip newTrip = new Trip(tripAttributes.get(0), tripAttributes.get(1), tripAttributes.get(2), tripAttributes.get(3), tripAttributes.get(4), 
//          tripAttributes.get(5), tripAttributes.get(6), tripAttributes.get(7), tripAttributes.get(8) );
//          dataSearched.add(newTrip);
//        }
//      }
//      float pickupTime = reader.getFloat(row, pickupColumn);
//      float dropoffTime = reader.getFloat(row, dropoffColumn);
//      if (pickupTime!=0&&dropoffTime!=0)    //To catch invalid/empty times.
//      {
//        float longitude = reader.getFloat(row, longColumn);
//        float latitude = reader.getFloat(row, latColumn);
//        pickupTime= (pickupTime-(int)pickupTime)*24;  //To get time part of serial number. As a ratio of 24 hours eg) 6:00 (am) being 0.25 and 24:00 being 1.
//        dropoffTime= (dropoffTime-(int)dropoffTime)*24;  //To get time part of serial number.
//        updatePickupArrayList(pickupTime, longitude, latitude);
//        updateDropoffArrayList(dropoffTime, longitude, latitude);
//      }
//    }
//    this.dataSearched=dataSearched;
//  }
//}
//

