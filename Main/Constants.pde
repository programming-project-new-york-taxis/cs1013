
final int SCREENX = 800;
final int SCREENY = 600;
final int GAP= SCREENY/5;
final int XLS_ROW_LIMIT=65535;

final int WIDGET_LENGTH = SCREENX/4;
final int WIDGET_WIDTH= SCREENX/20;
final int WIDGET_X = (SCREENX/2)-(WIDGET_LENGTH/2);
final int WIDGET_Y= GAP;

final int  EVENT_BUTTON1  =  1;  
final int  EVENT_BUTTON2  =  2;  
final int EVENT_BUTTON3 = 3;
final int EVENT_BUTTON4 = 4;
final int EVENT_BUTTON5 = 5;
final int EVENT_BUTTON6 = 6;
final int EVENT_BUTTON7 = 7;
final int EVENT_BUTTON8 = 8;
final int EVENT_BUTTON9 = 9;
final int EVENT_BUTTON10 = 10;
final int EVENT_BUTTON11 = 11;
final int EVENT_BUTTON12 = 12;
final int EVENT_BUTTON16 = 16;
final int EVENT_BUTTON17 = 17;
final int EVENT_BUTTON18 = 18;
final int EVENT_BUTTON19 = 19;
final int EVENT_BUTTON20 = 20;
final int EVENT_BUTTON21 = 21;
final int EVENT_BUTTON22 = 22;
final int EVENT_BUTTON23 = 23;
final int EVENT_BUTTON24 = 24;
final int EVENT_BUTTON25 = 25;

final int  EVENT_NULL  =  0;  
final color red = color(255, 0, 0);
final color green =color(0, 255, 0);
final color blue =color(20, 55, 255);
final color grey = color(190, 190, 190);
final color white = color(255);

final color firstBar = color(255, 0, 0);
final color secondBar =color(0, 255, 0);
final color thirdBar =color(0, 0, 255);
