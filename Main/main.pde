// Author/s: Everyone worked on this code in some way.
// Please note: A lot of the different classes have addtions, changes or bug fixes made by the whole team. 

import de.bezier.data.*;
import controlP5.*;

ArrayList widgetList;
PFont stdFont;
ArrayList screens;
Widget widget1, widget2, widget3, widget4, widget5, widget6, widget7, widget8, widget9, widget10, widget11, widget12, widget13, widget14, widget15, widget16, widget17, widget18, widget19, 
widget20, widget21, widget22, widget23;
Screen screen1, screen2, screen3, screen4, screen0, screen5, screen6, screen7, screen8, screen9, screen10, screen11;
int currentScreenNo=0;
int queryForData;
int query;
int event;
ArrayList<Float> distanceData=new ArrayList<Float>();
ArrayList <Trip> tripData;
ArrayList <Float> pickupData;
ArrayList <Float> dropOffData;
Chart barChart1;
Chart barChart2;
Chart barChart3;
Map map1;
Data data;
UserInput input;


void setup() {
  size(SCREENX, SCREENY);
  data = new Data(this);
  data.getMapData();

  screen0 = new Screen(0); //main screen
  screen1 = new Screen(1); // medailion, texi vendor, rate code
  screen2 = new Screen(2); // pickups, dropoff
  screen3 = new Screen(3); // picksups map 
  screen4 = new Screen(4); // dropoffs map
  screen5 = new Screen(5); // search box
  screen6 = new Screen(6); // distance, time, passengers
  screen7 = new Screen(7); // distance
  screen8 = new Screen(8); // time
  screen9 = new Screen(9); // passengers
  screen10 = new Screen(10); // time of day for pick up map
  screen11 = new Screen(11); // time of day for drop off map

  screens = new ArrayList();
  screens.add(screen0);
  screens.add(screen1);
  screens.add(screen2);
  screens.add(screen3);
  screens.add(screen4);
  screens.add(screen5);
  screens.add(screen6);
  screens.add(screen7);
  screens.add(screen8);
  screens.add(screen9);
  screens.add(screen10);
  screens.add(screen11);
  size(SCREENX, SCREENY);


  input = new UserInput();
}




void draw() {
  
  
//  if (currentScreenNo==2)
//  {
//    onMap=false;
//  }
//    if (currentScreenNo==3)
//    {
//     
//      if (event==16)
//      {
//        pickupData = data.getPickupPeriod1();
//      } else if (event==17)
//      {
//        pickupData = data.getPickupPeriod2();
//      } else if (event==18)
//      {
//        pickupData = data.getPickupPeriod3();
//      } else if (event==19)
//      {
//        pickupData = data.getPickupPeriod4();
//      }
//      println(pickupData.size());
//      map1 = new Map( this, pickupData);
//      onMap=true;
//    } else if (currentScreenNo==4)
//    {
//    
//      if (event==16)
//      {
//        dropOffData = data.getDropOffPeriod1();
//      } else if (event==17)
//      {
//        dropOffData = data.getDropOffPeriod1();
//      } else if (event==18)
//      {
//        dropOffData = data.getDropOffPeriod1();
//      } else if (event==19)
//      {
//        dropOffData = data.getDropOffPeriod1();
//      }
//    map1 = new Map( this, dropOffData);    
//     onMap=true;
//    }
    Screen currentScreen= (Screen) screens.get(currentScreenNo);
    currentScreen.draw();
}
void keyPressed() {
  if ( currentScreenNo == 5)
  {
    if (key == '\n' ) {
      saved = typing;
      println(saved);
      typing = ""; 
      currentScreenNo = 6;
     
      tripData=data.getUserSearch(queryForData, saved);
      println("trip Data size : "+tripData.size());
      
      if (tripData!=null) {
      ArrayList <Float> distanceData = data.getAverageDistance(tripData);
      barChart1 = new Chart("The Shortest", "The Average", "The Longest", "Distance (Kilometers)", distanceData.get(0), distanceData.get(1), distanceData.get(2));//DISTANCE
      ArrayList <Float> timeData = data.getAverageTime(tripData);
      barChart2 = new Chart("The Shortest", "The Average", "The Longest", "Time (in minutes)", timeData.get(0), timeData.get(1), timeData.get(2));//TIME
      ArrayList <Float> passengerData = data.getTotalPassengers(tripData);
      barChart3 = new Chart("Least", "Average", "Most", "Passengers", passengerData.get(0), passengerData.get(1), passengerData.get(2));//PASSENGERS
    } else
    {
      barChart1 = new Chart("The Shortest", "The Average", "The Longest", "Distance (Kilometers)", 0, 0, 0);//DISTANCE
      barChart2 = new Chart("The Shortest", "The Average", "The Longest", "Time (in minutes)", 0, 0, 0);//TIME
      barChart3 = new Chart("Least", "Average", "Most", "Passengers", 0, 0, 0);//PASSENGERS
    }
    } else {

      typing = typing + key;
    }
  }
}
void mousePressed()
{

  Screen currentScreen= (Screen) screens.get(currentScreenNo);
  event=currentScreen.getEvent(mouseX, mouseY);
  switch(event)
  {
  case 1:
    currentScreenNo=1;
    query=1;
    break;
  case 2:
    currentScreenNo=2;
    query=2;
    break;
  case 3:
    currentScreenNo=10;
    query=3;
    break;
  case 4:
    currentScreenNo=11;
    query=4;
    break;
  case 5:
    currentScreenNo=0;
    query=5;
    break; 
  case 6:
    currentScreenNo=5;
    query=6;
    queryForData = 1; 
    break;
  case 7:
    currentScreenNo=7;
    query=7;
    break;
  case 8:
    currentScreenNo=8;
    query=8;
    break; 
  case 9:
    currentScreenNo=6;
    query=9;
    break;
  case 10:
    currentScreenNo=7;
    query=10;
    break;
  case 11:
    currentScreenNo=8;
    query=11;
    break; 
  case 12:
    currentScreenNo=9;
    query=12;
    break;
  case 13:
    currentScreenNo=currentScreenNo-1;
    query=13;
    break;
  case 16:
    currentScreenNo=3;
    ArrayList <Float> pickupData = data.getPickupPeriod1();
    println(pickupData);
    map1 = new Map( this, pickupData);
    break;
  case 17:
    currentScreenNo=3;
    ArrayList <Float> pickupData2 = data.getPickupPeriod2();
    map1 = new Map( this, pickupData2);
    println(pickupData2.size());
    break;
  case 18:
    currentScreenNo=3;
    ArrayList <Float> pickupData3 = data.getPickupPeriod3();
    map1 = new Map( this, pickupData3);
    println(pickupData3.size());
    break;
  case 19:
    currentScreenNo=3;
    ArrayList <Float> pickupData4 = data.getPickupPeriod4();
    map1 = new Map( this, pickupData4);
    println(pickupData4.size());
    break;
  case 20:
    currentScreenNo=4;
    ArrayList <Float> dropOffData = data.getDropOffPeriod1();
    map1 = new Map( this, dropOffData);
     println(dropOffData);
    break;
  case 21:
    currentScreenNo=4;
    ArrayList <Float> dropOffData2 = data.getDropOffPeriod2();
    map1 = new Map( this, dropOffData2);
    break;
  case 22:
    currentScreenNo=4;
    ArrayList <Float> dropOffData3 = data.getDropOffPeriod3();
    map1 = new Map( this, dropOffData3);
    break;
  case 23:
    currentScreenNo=4;
    ArrayList <Float> dropOffData4 = data.getDropOffPeriod4();
    map1 = new Map( this, dropOffData4);
    break;
//      case 16:
//        currentScreenNo=3;
//        break;
//      case 17:
//        currentScreenNo=3;
//        break;
//      case 18:
//        currentScreenNo=3;
//        break;
//      case 19:
//        currentScreenNo=3;
//        break;
//      case 20:
//        currentScreenNo=4;
//        break;
//      case 21:
//        currentScreenNo=4;
//        break;
//      case 22:
//        currentScreenNo=4;
//    
//        break;
//      case 23:
//        currentScreenNo=4;
//        break;
  case 24:
    currentScreenNo=5;
    query=24;
    queryForData = 2; 
    break;

  case 25: 
    currentScreenNo=5;
    query=24;
    queryForData = 3; 
    break;
  default:
  }
}




