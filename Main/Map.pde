// Author/s: Sofiane Echaib handled the implementation of the map 

import de.fhpotsdam.unfolding.*;
import de.fhpotsdam.unfolding.geo.*;
import de.fhpotsdam.unfolding.utils.*;
import de.fhpotsdam.unfolding.providers.*;
import processing.core.*;
import de.fhpotsdam.unfolding.marker.*;
//ArrayList <Location> locationList = new ArrayList <Location>();


class Map {

  UnfoldingMap map;
  PApplet parent;

  Map(PApplet parent, ArrayList <Float> longLatData) {

    this.parent = parent;
  
    size(800, 600);
    map = new UnfoldingMap(parent, new Google.GoogleMapProvider() ) ;
    MapUtils.createDefaultEventDispatcher(parent, map);
    Location newYorkLocation = new Location(40.712784f, -74.005941f);
    map.zoomAndPanTo(newYorkLocation, 10);
    float maxPanningDistance = 30; // in km
    map.setPanningRestriction(newYorkLocation, maxPanningDistance);
    
   
   for ( int i = 0; i < longLatData.size(); i = i + 2)
     {
         float tempLong = longLatData.get(i);
         float tempLat = longLatData.get(i + 1);
         
       
           Location location = new Location(tempLat, tempLong);
//          locationList.add(location);
            // Create point markers for locations
            Marker marker = new Marker(location);
//         SimplePointMarker marker = new SimplePointMarker(location);
               
           // Add markers to the map
           map.addMarkers(marker);
    
     }  

  }

  void draw() 
  {
   map.draw();
   }
}

