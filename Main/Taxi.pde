// Author/s: Mainly Sean Durban handled the implementation of data extraction. 

class Taxi{
private String medallion;
private String hack_license; 
private String vendor_id;
private int rate_code;
private String store_and_fwd_flag;
private Float pickup_datetime;
private Float dropoff_datetime;
private Float passenger_count;
private Float trip_time;
private Float trip_distance;
private Float pickup_latitude;
private Float pickup_longitude;
private Float dropoff_latitude;
private Float dropoff_longitude;

Taxi(String medallion,String hackLicence,String vendorID,int rateCode,String stFlag,Float pickup,Float dropOff,Float passCount,
Float tripTime,Float tripDistance,Float pickUpLong,Float pickUpLat,Float dropOffLong, Float dropOffLat){
  this.medallion=medallion;
  this.hack_license=hackLicence;
  this.vendor_id= vendorID;
  this.rate_code= rateCode;
  this.store_and_fwd_flag=stFlag;
  this.pickup_datetime=pickup;
  this.dropoff_datetime=dropOff;
   this.passenger_count= passCount;
   this.trip_time=tripTime;
   this.trip_distance=tripDistance;
   this.pickup_latitude=pickUpLat;
   this.pickup_longitude=pickUpLong;
   this.dropoff_latitude=dropOffLong;
   this.dropoff_longitude=dropOffLat;
  
}

public String getMedallion(){
 return this.medallion;  
}

public String getVendor(){
 return this.vendor_id;  
}

public int getRateCode(){
 return this.rate_code;  
}

public Float getPickupTime(){
 return this.pickup_datetime;  
}

public Float getDropoffTime(){
 return this.dropoff_datetime;  
}


public Float getPickupLong(){
 return this.pickup_longitude;  
}


public Float getPickupLat(){
 return this.pickup_latitude;  
}


public Float getDropoffLong(){
 return this.dropoff_longitude;  
}

public Float getDropoffLat(){
 return this.dropoff_latitude;  
}

//public double getTripDistance(){
//  double tripD = Double.parseDouble(this.trip_distance);
//  return tripD;
//}

Float getCurrentAttribute(int attributeNo)  //Gets attributes for trip object.
{
 Float currentAttribute =0.0;
  switch(attributeNo)
  {
   case 1:
    currentAttribute = this.pickup_datetime;
    break;
  case 2:
    currentAttribute = this.dropoff_datetime;
    break;
 case 3:
    currentAttribute = this.passenger_count;
    break;
 case 4:
    currentAttribute = this.trip_time;
    break;
 case 5:
    currentAttribute = this.trip_distance;
    break; 
 case 6:
    currentAttribute = this.pickup_latitude;
    break;
 case 7:
   currentAttribute = this.pickup_longitude;
    break;
 case 8:
    currentAttribute = this.dropoff_latitude;
    break; 
 case 9:
   currentAttribute = this.dropoff_longitude;
    break;
default:
      break;
  }
  return currentAttribute;
  }

 void draw(){

 }
 
 
  String toString(){
  String space = " ||  ";
  String toPrint =this.medallion+space+this.hack_license+this.vendor_id+space+ this.rate_code+space+this.store_and_fwd_flag+space+ this.pickup_datetime+space+this.dropoff_datetime+space
  + this.passenger_count+space+ this.trip_time+space+ this.trip_distance+space+ this.pickup_latitude+space+this.pickup_longitude+space+this.dropoff_latitude+space+this.dropoff_longitude;
  return toPrint;
  }
  
}
