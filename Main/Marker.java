// Author/s: Sofiane Echaib handled the implementation of the map 

import processing.core.*;
import de.fhpotsdam.unfolding.geo.Location;
import de.fhpotsdam.unfolding.marker.SimplePointMarker;

class Marker extends SimplePointMarker {
  
  Marker(Location location){
    
    super(location); 
  }
  


public void draw (PGraphics pg, float x, float y){
 
//   pg.strokeWeight(6);
//   pg.stroke(255, 0, 0, 5); 
//   pg.noFill();
   pg.fill(0);
   pg.ellipse(x, y, 15, 15);
  
  }
  
  
}



