// Author/s: Mainly Sean Durban handled the implementation of data extraction. 

class Trip {
  private Float store_and_fwd_flag;
  private Float pickup_datetime;
  private Float dropoff_datetime;
  private Float passenger_count;
  private Float trip_time;
  private Float trip_distance;
  private Float pickup_latitude;
  private Float pickup_longitude;
  private Float dropoff_laitude;
  private Float dropoff_longitude;


  Trip(Float pickup, Float dropOff, Float passCount, 
  Float tripTime, Float tripDistance, Float pickUpLong, Float pickUpLat, Float dropOffLong, Float dropOffLat) {


    this.pickup_datetime=pickup;
    this.dropoff_datetime=dropOff;
    this.passenger_count= passCount;
    this.trip_time=tripTime;
    this.trip_distance=tripDistance;
    this.pickup_latitude=pickUpLat;
    this.pickup_longitude=pickUpLong;
    this.dropoff_laitude=dropOffLong;
    this.dropoff_longitude=dropOffLat;
  }
  String tofloat() {
    String space = " ||  ";
    String toPrint =this.store_and_fwd_flag+space+ this.pickup_datetime+space+this.dropoff_datetime+space
      + this.passenger_count+space+ this.trip_time+space+ this.trip_distance+space+ this.pickup_latitude+space+this.pickup_longitude+space+this.dropoff_laitude+space+this.dropoff_longitude;
    return toPrint;
  }
  Float getDistance()
  {
    return this.trip_distance;
  }
  Float getTime()
  {
    return this.trip_time;
  }
  Float getPassengerNo()
  {
    return this.passenger_count;
  }
  Float getPickUpLong()
  {
    return this.pickup_longitude;
  }
  Float getPickUpLat()
  {
    return this.pickup_latitude;
  }
}

