// Author/s: Sam Green, Evan O'Sullivan and Peter O'Leary handled widget creation.

class  Widget {  
  int  x, y, width, height;  
  String  label;  
  int  event;  
  color  widgetColor, labelColor, widgetStroke;  
  PFont  widgetFont;  

  Widget(int  x, int  y, int  width, int  height, String  label, 
  color  widgetColor, PFont  widgetFont, int  event) {  
    this.x=x+3;  
    this.y=y+3;  
    this.width  =  width-7;  
    this.height=  height-7;  
    this.label=label;  
    this.event=event;    
    this.widgetColor=widgetColor;  
    this.widgetFont=widgetFont;  
    labelColor  =  color(0);  
    widgetStroke = color(255);
  }  
  void  draw() {  

    fill(widgetColor);  
    strokeWeight(1.5);
    stroke(widgetStroke);
    rect(x, y, width, height);  
    fill(labelColor);  

    text(label, x+WIDGET_LENGTH/3, y+WIDGET_WIDTH/2);
  }  
  int  getEvent(int  mX, int  mY) {  
    if (mX>x  &&  mX  <  x+width  &&  mY  >y  &&  mY  <y+height) {  
      return  event;
    }  
    return  EVENT_NULL;
  }
}
