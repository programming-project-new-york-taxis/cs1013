import controlP5.*;

ControlP5 searchBox;

String textValue = "";

void setup() {
  size(700,400);
  
  PFont font = createFont("arial",20);
  
  searchBox = new ControlP5(this);             
  searchBox.addTextfield("Type your search and press enter")
     .setPosition(20,170)
     .setSize(200,40)
     .setFont(createFont("arial",20))
     .setAutoClear(false)
     ;
     
  textFont(font);
}

void draw() {
  background(0);
  fill(255);
//  text(textValue, 360,180);
}

void controlEvent(ControlEvent theEvent) {
  if(theEvent.isAssignableFrom(Textfield.class)) {
    String answer = theEvent.getStringValue();
    println(answer);
  }
}
